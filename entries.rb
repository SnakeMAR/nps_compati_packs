require 'csv'
require 'open-uri'

begin
  
games = CSV.parse(open("https://docs.google.com/spreadsheets/d/18HHxaQhGgqDjH2mIgC3T1OVWNRr_4HHfuLsYmiKXnfk/export?format=tsv&id=18HHxaQhGgqDjH2mIgC3T1OVWNRr_4HHfuLsYmiKXnfk&gid=1375137430"), { :col_sep => "\t", :headers => true, encoding: "ISO8859-1"})
demos = CSV.parse(open("http://nopaystation.com/tsv/PSV_DEMOS.tsv"), { :col_sep => "\t", :headers => true })


result = ""
Dir["PCS*/*.ppk"].each do |game|
	game_title_id = game.split("/")[0]
  begin
    game_name = games.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
  rescue
    game_name = demos.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
  end
	result += "#{game}=#{game_name}\n"
end

File.write("entries.txt", result)
rescue
end
